# Java-based Binary Data Viewer #

BitView is a viewer for binary data. It is useful for examining files at a binary level such as ham radio data, network data, executables, or other binary files. BitView allows the data to be viewed on any width, at any start bit, and for operations such as bit inversions or bit skipping. Image files can be viewed, and text data can be decoded using various encoding schemes.
