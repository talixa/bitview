package com.talixa.bitview.frames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.talixa.bitview.shared.BitViewConstants;
import com.talixa.bitview.widgets.BitViewWidget;

public class FrameSettings {
	
	private static final int WIDTH = 300;
	private static final int HEIGHT = 200;
	
	public static void createAndShowGUI(final BitViewWidget bv) {
		// Create frame
		final JFrame frame = new JFrame(BitViewConstants.TITLE_SETTINGS);
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	
		// cones color panel
		JPanel panelOnes = new JPanel(new FlowLayout());
		panelOnes.add(new JLabel("Ones Color: "));
		final JButton onesButton = new JButton();
		onesButton.setPreferredSize(new Dimension(80,20));
		onesButton.setBackground(bv.getOnesColor());
		onesButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				Color newColor = JColorChooser.showDialog(frame, "Choose Ones Color", bv.getOnesColor());
				if (newColor != null) {
					bv.setOnesColor(newColor);			
					onesButton.setBackground(newColor);
				}
			}
		});			
		panelOnes.add(onesButton);
		
		// zero color panel
		JPanel panelZero = new JPanel(new FlowLayout());
		panelZero.add(new JLabel("Zero Color: "));
		final JButton zeroButton = new JButton();
		zeroButton.setPreferredSize(new Dimension(80,20));
		zeroButton.setBackground(bv.getZeroColor());
		zeroButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				Color newColor = JColorChooser.showDialog(frame, "Choose Zero Color", bv.getZeroColor());
				if (newColor != null) {
					bv.setZeroColor(newColor);			
					onesButton.setBackground(newColor);
				}
			}
		});			
		panelZero.add(zeroButton);		
		
		// speed panel
		JPanel speedPanel = new JPanel(new FlowLayout());
		speedPanel.add(new JLabel("Play Speed (ms): "));
		final JTextField playSpeed = new JTextField(5);
		playSpeed.setText(String.valueOf(bv.getPlaySpeed()));
		speedPanel.add(playSpeed);				
		
		// filesize panel
		JPanel sizePanel = new JPanel(new FlowLayout());
		sizePanel.add(new JLabel("Max filesize (kb): "));
		final JTextField fileSize = new JTextField(5);
		fileSize.setText(String.valueOf(bv.getMaxFileSize()));
		sizePanel.add(fileSize);
		
		// border panel
		JPanel borderPanel = new JPanel(new FlowLayout());
		final JCheckBox borderStatus = new JCheckBox("Show Border: ", bv.getDisplayBorder());
		borderPanel.add(borderStatus);
				
		// add to main panel
		JPanel panelMain = new JPanel(new GridLayout(5,1));
		panelMain.add(panelOnes);
		panelMain.add(panelZero);
		panelMain.add(speedPanel);
		panelMain.add(sizePanel);
		panelMain.add(borderPanel);
		
		JButton okButton = new JButton(BitViewConstants.LABEL_OK);
		okButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					bv.setPlaySpeed(Integer.parseInt(playSpeed.getText()));
				} catch (NumberFormatException nfe) {
					bv.setPlaySpeed(250);
				}
				try {
					bv.setMaxFileSize(Integer.parseInt(fileSize.getText()));
				} catch (NumberFormatException nfe) {
					bv.setMaxFileSize(50);
				}
				bv.setDisplayBorder(borderStatus.isSelected());
				frame.dispose();
			}
		});
								
		// Add to frame
		frame.add(panelMain, BorderLayout.CENTER );
		frame.add(okButton, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);						
	}	
}
