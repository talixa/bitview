package com.talixa.bitview.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.Border;

import com.talixa.bitview.listeners.ExitActionListener;
import com.talixa.bitview.shared.BitViewConstants;
import com.talixa.bitview.shared.IconHelper;

public class FrameAbout {
	
	private static final int WIDTH = 520;
	private static final int HEIGHT = 220;
	private static final int BORDER = 10;
	
	private static final String ABOUT_CONTENTS = 
			"<html><center>BitView is a Java-based binary data viewer. It can be used to view " +
		    "files, data dumps, demodulated radio data, or other binary data from various sources. " +
		    "Data contents can be modified or viewed in various encoding schemes. " +
			"<br><br>Version: " + BitViewConstants.VERSION + 
			"<br>Copyright 2017, Talixa Software & Service, LLC</center></html>";
	
	public static void createAndShowGUI(JFrame owner) {
		// Create dialog
		JDialog dialog = new JDialog(owner, BitViewConstants.TITLE_ABOUT);
		dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// set icon
		IconHelper.setIcon(dialog);		
		
		// set escape listener
		addEscapeListener(dialog);
		
		// set modality
		dialog.setModal(true);
		
		// create a content holder with a 10 pixel border
		JPanel holder = new JPanel();
		holder.setLayout(new BorderLayout());
		holder.setBorder(BorderFactory.createEmptyBorder(BORDER, BORDER, BORDER, BORDER));
		dialog.add(holder);
	
		// create components
		ClassLoader cl = IconHelper.class.getClassLoader();	
		JLabel icon = new JLabel(new ImageIcon(cl.getResource(BitViewConstants.ICON)));
		holder.add(icon, BorderLayout.WEST);
		
		JLabel text = new JLabel(ABOUT_CONTENTS);
		Border padding = BorderFactory.createEmptyBorder(BORDER, BORDER, BORDER, BORDER);
		text.setBorder(padding);
		JButton ok = new JButton(BitViewConstants.LABEL_OK);
		ok.addActionListener(new ExitActionListener(dialog));
				
		// Add to frame
		holder.add(text, BorderLayout.CENTER);
		holder.add(ok, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		dialog.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		dialog.pack();
		dialog.setLocation(left,top);
		dialog.setVisible(true);						
	}	
	
	public static void addEscapeListener(final JDialog dialog) {
	    ActionListener escListener = new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	            dialog.dispose();
	        }
	    };

	    dialog.getRootPane().registerKeyboardAction(escListener,
	            KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
	            JComponent.WHEN_IN_FOCUSED_WINDOW);
	}	
}
