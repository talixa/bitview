package com.talixa.bitview.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.talixa.bitview.listeners.ExitActionListener;
import com.talixa.bitview.shared.BitViewConstants;

public class FrameOperations {
	
	private static final int WIDTH = 400;
	private static final int HEIGHT = 210;
	private static final int BORDER = 10;
	
	private static final String HELP_CONTENTS = 
			"<html><center>Operations allow you to modify the data. " +
			"For example, a typical RTTY signal contains start and stop " +
			"bits which are not necessary to decode the underlying data " +
			"Other signals may be received inverted. Operations allow " +
			"you to fix these issues. Operations available include take (t), " +
			"skip (s), and invert (i). Each operation is followed by the " +
			"number of bits. For example, to skip start and stop bits on " +
			"an RTTY signal that uses 5-bit baudot, s1t5s1 to skip 1, take 5, " +
			"take 1. If the data was inverted, use s1i5s1 to skip 1, invert 5, " +
			"skip 1.</center></html>";
	
	public static void createAndShowGUI() {
		// Create frame
		JFrame frame = new JFrame(BitViewConstants.TITLE_OPERATIONS);
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	
		// create components
		JLabel text = new JLabel(HELP_CONTENTS);	
		text.setBorder(BorderFactory.createEmptyBorder(BORDER, BORDER, BORDER, BORDER));
		JButton ok = new JButton(BitViewConstants.LABEL_OK);
		ok.addActionListener(new ExitActionListener(frame));
				
		// Add to frame
		frame.add(text, BorderLayout.CENTER);
		frame.add(ok, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);						
	}	
}
