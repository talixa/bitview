package com.talixa.bitview.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.talixa.bitview.shared.BitViewConstants;

public class FrameImageViewer {
	
	private static final int PADDING_HEIGHT = 25;
	private static final int PADDING_WIDTH = 15;
	
	public static void createAndShowGUI(byte[] imageData) {
		// Create frame
		JFrame frame = new JFrame(BitViewConstants.TITLE_IMAGE_VIEWER);
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// Image display area		
		JLabel imagePanel = null;
		try {			
			// load image
			ImageIcon image = new ImageIcon(imageData);
			int width = image.getIconWidth();
			int height = image.getIconHeight();
			
			if (width < 100) width = 100;
			if (height < 100) height = 100;
			
			// create panel
			imagePanel = new JLabel(new ImageIcon(imageData));
			frame.add(imagePanel, BorderLayout.CENTER);		
			
			// Set location and display
			Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
			frame.setPreferredSize(new Dimension(width+PADDING_WIDTH,height+PADDING_HEIGHT));
			int left = (screenSize.width/2) - (width/2);
			int top  = (screenSize.height/2) - (height/2);
			frame.pack();
			frame.setLocation(left,top);
			frame.setVisible(true);							
		} catch (NullPointerException npe) {
			JOptionPane.showMessageDialog(frame, BitViewConstants.ERROR_BAD_IMAGE);
			frame.dispose();
		}				
	}
}
