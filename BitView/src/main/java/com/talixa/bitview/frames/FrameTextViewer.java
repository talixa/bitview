package com.talixa.bitview.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

import com.talixa.bitview.encoding.BaudotDecoder;
import com.talixa.bitview.encoding.BaudotDecoder.Mode;
import com.talixa.bitview.shared.BitViewConstants;

public class FrameTextViewer {
		
	private static final String[] CODES = {"ASCII", "EBCDIC", "Baudot - US", "Baudot - ITU", "Baudot - US - Rev", "Baudot - ITU - Rev", "Unicode"};
	public static void createAndShowGUI(final byte[] data) {
		// Create frame
		final JFrame frame = new JFrame(BitViewConstants.TITLE_TEXT_VIEWER);
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// Text display area
		final JTextArea text = new JTextArea();
		text.setLineWrap(true);
		
		// Create options
		
		final JComboBox textOptions = new JComboBox(CODES);
		textOptions.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {								
				try {
					switch(textOptions.getSelectedIndex()) {
						case 0: text.setText(new String(data, "ASCII")); break;
						case 1: text.setText(new String(data, "Cp1047")); break;
						case 2: text.setText(new BaudotDecoder(data,Mode.US).decode()); break;
						case 3: text.setText(new BaudotDecoder(data,Mode.CCITT2).decode()); break;
						case 4: text.setText(new BaudotDecoder(data,Mode.REV_US).decode()); break;
						case 5: text.setText(new BaudotDecoder(data,Mode.REV_CCITT2).decode()); break;
						case 6: text.setText(new String(data, "UNICODE")); break;
						default: break;
					}
					text.setCaretPosition(0);
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(frame, BitViewConstants.ERROR_ENCODING);
				}				
			}
		});
		
		// create toolbar		
		JToolBar toolbar = new JToolBar();
		toolbar.add(new JLabel("Coding: "));
		toolbar.add(textOptions);
		
		// create text area
						
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setViewportView(text);
		text.setAutoscrolls(true);
		frame.add(scrollPane, BorderLayout.CENTER);		
		
		try {
			text.setText(new String(data, "ASCII"));
		} catch (UnsupportedEncodingException e) {
			// DO NOTHING
		}
		text.setCaretPosition(0);
				
		// Add to frame
		frame.add(toolbar, BorderLayout.NORTH);
		frame.add(scrollPane, BorderLayout.CENTER);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(BitViewConstants.APP_WIDTH/2,BitViewConstants.APP_HEIGHT/2));
		int left = (screenSize.width/2) - (BitViewConstants.APP_WIDTH/4);
		int top  = (screenSize.height/2) - (BitViewConstants.APP_HEIGHT/4);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);						
	}
}
