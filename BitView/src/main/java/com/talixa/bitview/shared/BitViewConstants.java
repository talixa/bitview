package com.talixa.bitview.shared;

public class BitViewConstants {
	public static final String APPNAME = "BitView";
	public static final String VERSION = "2.0";
	
	public static final String TITLE_MAIN = APPNAME + " " + VERSION;
	public static final String TITLE_ABOUT = "About " + APPNAME;
	public static final String TITLE_OPERATIONS = "Operations Help";
	public static final String TITLE_SETTINGS = APPNAME + " Settings";
	public static final String TITLE_IMAGE_VIEWER = APPNAME + " Image Viewer";
	public static final String TITLE_TEXT_VIEWER = APPNAME + " Text Viewer";
	
	public static final String MENU_FILE = "File";
	public static final String MENU_HELP = "Help";
	public static final String MENU_EXIT = "Exit";
	public static final String MENU_ABOUT = "About";
	public static final String MENU_OPERATIONS = "Operations";
	public static final String MENU_OPEN = "Open";
	public static final String MENU_SETTINGS = "Settings";
	public static final String MENU_EXPORT = "Export";
	
	public static final String LABEL_OK = "Ok";
	public static final String LABEL_CANCEL = "Cancel";
	
	public static final int APP_WIDTH = 800;
	public static final int APP_HEIGHT = 600;
	
	public static final int DEFAULT_WIDTH = 8;
	public static final int DEFAULT_START = 0;
	
	public static final String ICON = "res/bitview.png";
	
	public static final String ERROR_FILE_LOAD = "Error loading file";
	public static final String ERROR_BAD_IMAGE = "Not a recognized image type";
	public static final String ERROR_MAX_FILESIZE = "Max filesize exceeded";
	public static final String ERROR_ENCODING = "Encoding error";
}
