package com.talixa.bitview.listeners;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextField;

import com.talixa.bitview.widgets.BitViewWidget;

public class BitViewKeyListener implements KeyListener {
	
	public static final int SET_WIDTH = 0;
	public static final int SET_START = 1;
	
	private int function;
	private BitViewWidget bv;
	private JTextField text;
	
	public BitViewKeyListener(int function, BitViewWidget bv, JTextField associatedText) {
		this.function = function;
		this.bv = bv;
		this.text = associatedText;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// DO NOTHING
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// DO NOTHING		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		try {
			switch (function) {
				case SET_START: bv.setStartBit(Integer.parseInt(text.getText())); break;
				case SET_WIDTH: bv.setDisplayWidth(Integer.parseInt(text.getText())); break;
				default: 		break;
			}
		} catch (NumberFormatException nfe) {
			// DO NOTHING
		}
	}
}
