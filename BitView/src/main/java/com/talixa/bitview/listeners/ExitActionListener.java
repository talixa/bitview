package com.talixa.bitview.listeners;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ExitActionListener implements ActionListener {
	
	private Window window;
	
	public ExitActionListener(Window w) {
		this.window = w;
	}
	
	public void actionPerformed(ActionEvent e) {	
		window.dispose();
	}	
}
