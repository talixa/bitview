package com.talixa.bitview.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.talixa.bitview.BitView;
import com.talixa.bitview.interfaces.WidthChangedListener;
import com.talixa.bitview.shared.BitViewConstants;
import com.talixa.bitview.widgets.BitViewWidget;

public class BitViewButtonListener implements ActionListener {

	public static final int INCREASE_WIDTH = 0;
	public static final int DECREASE_WIDTH = 1;
	public static final int INCREMENT_START = 2;
	public static final int DECREMENT_START = 3;
	public static final int INCREMENT_SIZE = 4;
	public static final int DECREMENT_SIZE = 5;
	public static final int PLAY = 6;
	public static final int STOP = 7;
	public static final int APPLY_OPERATIONS = 8;
	public static final int EXPORT = 9;
	
	private int function;
	private BitViewWidget bv;
	private JTextField text;
	
	public BitViewButtonListener(int function, BitViewWidget bv) {
		this.function = function;
		this.bv = bv;
	}
	
	public BitViewButtonListener(int function, BitViewWidget bv, JTextField associatedText) {
		this.function = function;
		this.bv = bv;
		this.text = associatedText;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (function) {
			case INCREASE_WIDTH:	bv.increaseWidth();	
									text.setText(String.valueOf(bv.getDisplayWidth()));
									break;
			case DECREASE_WIDTH:	bv.decreaseWidth();	
									text.setText(String.valueOf(bv.getDisplayWidth()));
									break;
			case INCREMENT_START: 	bv.increaseStartBit();	
									text.setText(String.valueOf(bv.getStartBit()));
									break;
			case DECREMENT_START:	bv.decreaseStartBit();	
									text.setText(String.valueOf(bv.getStartBit()));
									break;
			case INCREMENT_SIZE:	bv.increaseSize();				
									break;
			case DECREMENT_SIZE:	bv.decreaseSize();		
									break;
			case PLAY:				bv.play(new WidthChangedListener() {				
										@Override
										public void onWidthChanged() {						
											text.setText(String.valueOf(bv.getDisplayWidth()));
										}
									});
									break;
			case STOP:				bv.stop();
									break;
			case APPLY_OPERATIONS:	bv.setOperations(expandOperations(text.getText()));
									break;
			case EXPORT:			saveData(bv.getCurrentData());
									break;
			default:				// DO NOTHING
									break;
		}		
	}
	
	private void saveData(byte[] data) {		
		int result = BitView.fileChooser.showSaveDialog(bv.getParent());
		if (result == JFileChooser.APPROVE_OPTION) {
			FileOutputStream fos = null;
			try {
				File f = BitView.fileChooser.getSelectedFile();
				fos = new FileOutputStream(f);
				fos.write(data);	
				fos.close();
			} catch (IOException exception) {
				JOptionPane.showMessageDialog(bv.getParent(), BitViewConstants.ERROR_FILE_LOAD);
			} finally {
				if (fos != null) {
					try {
						fos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	private String expandOperations(String baseOps) {
		String ops = new String();
		for(int opIndex = 0; opIndex < baseOps.length(); ++opIndex) {
			if(Character.isDigit(baseOps.charAt(opIndex))) {
				int opCount = Integer.valueOf(baseOps.charAt(opIndex)) - 48 - 1;
				for(int j = 0; j < opCount; ++j) {
					ops += ops.charAt(ops.length()-1);
				}
			} else {
				ops += baseOps.charAt(opIndex);
			}
		}
		return ops;
	}
}

