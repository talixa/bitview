package com.talixa.bitview.analysis;

import java.io.FileInputStream;
import java.io.IOException;

import com.talixa.bitview.analysis.data.BiasData;

public class Bias {

	/**
	 * Return the percentage of 1s in the input file
	 * 
	 * @param file name of file to analyze
	 * @return statistics for bias data
	 * @throws IOException 
	 */
	public static BiasData calculate(String file) throws IOException {
		// stream to read data from
		FileInputStream fis = null;
		
		// sum of ones and zeros in the file
		int onesCount = 0;
		int zeroCount = 0;
		
		try {
			// open file
			fis = new FileInputStream(file);
			
			// create buffer
			byte[] buff = new byte[512];
			int bytesRead = fis.read(buff);
			
			// read from file to buffer
			while (bytesRead > 0) {
				// iterate through each byte of buffer
				for(int i = 0; i < bytesRead; ++i) {
					// check each bit of byte
					for(int bit = 0; bit < 8; ++bit) {
						int value = (buff[i] >> bit) & 0x01;
						if (value == 1) {
							++onesCount;
						} else {
							++zeroCount;
						}
					}
				}
				bytesRead = fis.read(buff);
			}
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					// do nothing
				}
			}
		}

		return new BiasData(onesCount,zeroCount);
	}
}
