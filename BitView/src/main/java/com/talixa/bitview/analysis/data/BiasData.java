package com.talixa.bitview.analysis.data;

public class BiasData {

	private final int onesCount;
	private final int zerosCount;
	private final int totalCount;
	private final float onesBias;
	private final float zeroBias;
	
	public BiasData(final int onesCount, final int zerosCount) {
		this.onesCount = onesCount;
		this.zerosCount = zerosCount;
		this.totalCount = onesCount + zerosCount;
		this.onesBias =  (float)onesCount / (float)totalCount;
		this.zeroBias =  (float)zerosCount / (float)totalCount;
	}
	
	public int getOnesCount() {
		return onesCount;
	}
	
	public int getZerosCount() {
		return zerosCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public float getOnesBias() {
		return onesBias;
	}

	public float getZeroBias() {
		return zeroBias;
	}
	
	public String toString() {
		return String.format("Ones Count: %d - Zeros Count: %d - Ones Bias: %f - Zero Bias: %f", onesCount, zerosCount, onesBias, zeroBias);
	}
}
