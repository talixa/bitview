package com.talixa.bitview.widgets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

import org.apache.commons.io.FileUtils;

import com.talixa.bitview.interfaces.WidthChangedListener;
import com.talixa.bitview.shared.BitViewConstants;

@SuppressWarnings("serial")
public class BitViewWidget extends JPanel {
	
	private byte[] data = new byte[0];
	
	private int displayWidth = 8;
	private int startBit = 0;				
	private boolean displayBorder = true;
	private int bitSize = 9;
	private Color onesColor = Color.BLACK;
	private Color zeroColor = Color.WHITE;
	private String operations = "";
	private int playSpeed = 250;
	private int maxFileSize = 16;	// 16K
	private static final int KBYTE = 1024;
	
	public BitViewWidget() {
	}
	
	private void draw(Graphics g) {
		int byteCount = 0;
		int skippedBits = 0;
		for(byte b: data) {
			int bits[] = getBitArray(b);
			for(int i = 0; i < bits.length; ++i) {								
				int bitNum = (((byteCount*8)) + i) - startBit;
				if (bitNum >= 0) {
					int bitX = getBitX(bitNum-skippedBits);
					int bitY = getBitY(bitNum-skippedBits);
					
					int thisBit = bits[i];
					if (operations.length() > 0) {
						char op = getOperation(bitNum);
						switch (op) {
							case 't': break;
							case 's': ++skippedBits; continue;
							case 'i': thisBit = thisBit == 1 ? 0 : 1; break;							
						}
					}
					
					g.setColor(thisBit == 0 ? zeroColor : onesColor);
					g.fillRect(bitX, bitY, bitSize, bitSize);
					if (displayBorder) {
						// invert colors for border
						g.setColor(thisBit == 1 ? zeroColor : onesColor);
						g.drawRect(bitX, bitY, bitSize, bitSize);
					}
				}
			}		
			++byteCount;
		}
		this.setPreferredSize(new Dimension(displayWidth * bitSize, (((data.length * 8) - startBit - skippedBits) * bitSize)/displayWidth));
		revalidate();
	}
	
	private char getOperation(int bitNumber) {
		int opSize = operations.length();
		int opNum = bitNumber % opSize;
		return operations.charAt(opNum);
	}
	
	private int getBitX(int bitNum) {
		return (bitNum % displayWidth) * bitSize;
	}
	
	private int getBitY(int bitNum) {
		return (bitNum / displayWidth) * bitSize;
	}
	
	private int[] getBitArray(byte b) {
		int[] bits = new int[8];
		bits[0] = b >> 7 & 0x01;
		bits[1] = b >> 6 & 0x01;
		bits[2] = b >> 5 & 0x01;
		bits[3] = b >> 4 & 0x01;
		bits[4] = b >> 3 & 0x01;
		bits[5] = b >> 2 & 0x01;
		bits[6] = b >> 1 & 0x01;
		bits[7] = b >> 0 & 0x01;
		return bits;
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.draw(g);
	}
	
	public String getOperations() {
		return operations;
	}
	
	public void setOperations(String ops) {
		this.operations = ops;
		repaint();
	}
	
	public void setDisplayWidth(int width) {
		this.displayWidth = width;
		repaint();
	}
	
	public int getDisplayWidth() {
		return this.displayWidth;
	}
	
	public void setDisplayBorder(boolean displayBorder) {
		this.displayBorder = displayBorder;
		repaint();
	}
	
	public boolean getDisplayBorder() {
		return this.displayBorder;
	}
	
	public void setBitSize(int bitSize) {
		this.bitSize = bitSize;
	}
	
	public void setOnesColor(Color onesColor) {
		this.onesColor = onesColor;
		repaint();
	}
	
	public void setZeroColor(Color zeroColor) {
		this.zeroColor = zeroColor;
		repaint();
	}
	
	public Color getOnesColor() {
		return this.onesColor;
	}
	
	public Color getZeroColor() {
		return this.zeroColor;
	}
	
	public void setStartBit(int startBit) {
		this.startBit = startBit;
		repaint();
	}
	
	public void decreaseStartBit() {
		this.startBit = startBit - 1;
		if (startBit < 0) {
			startBit = 0;
		}
		repaint();
	}
	
	public void increaseStartBit() {
		this.startBit = startBit + 1;
		repaint();
	}
	
	public int getStartBit() {
		return this.startBit;
	}
	
	public void decreaseWidth() {
		this.displayWidth = displayWidth -1;
		if (displayWidth < 1) {
			displayWidth = 1;
		}
		repaint();
	}
	
	public void increaseWidth() {
		this.displayWidth = displayWidth +1;
		repaint();
	}
	
	public void setPlaySpeed(int speed) {
		this.playSpeed = speed;
	}
	
	public int getPlaySpeed() {
		return this.playSpeed;
	}
	
	public void decreaseSize() {
		this.bitSize = bitSize - 1;
		if (bitSize < 2) {
			bitSize = 2;
		}
		repaint();
	}
	
	public void increaseSize() {
		this.bitSize = bitSize + 1;
		repaint();
	}
	
	public void setData(byte[] data) {
		this.data = data;
		repaint();
	}
	
	public void setMaxFileSize(int maxFileSize) {
		this.maxFileSize = maxFileSize;
	}
	
	public int getMaxFileSize() {
		return this.maxFileSize;
	}
	
	public void setDataFromFile(File f) throws IOException {
		byte[] fileContents = FileUtils.readFileToByteArray(f);
		if (fileContents.length > (maxFileSize * KBYTE)) {
			data = Arrays.copyOfRange(fileContents, 0, maxFileSize * KBYTE);
			JOptionPane.showMessageDialog(this.getParent(), BitViewConstants.ERROR_MAX_FILESIZE);
		} else {
			data = fileContents;
		}
		
		repaint();		
	}	
	
	private boolean playing = false;
	
	public void play(final WidthChangedListener listener) {
		playing = true;
		
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
			protected Void doInBackground() {
				try {
					while (playing) {
						increaseWidth();
						listener.onWidthChanged();
						Thread.sleep(playSpeed);
					}
				} catch (InterruptedException e) {
					playing = false;
				}		
				return null;
			}
		};
		
		worker.execute();								
	}
	
	public void stop() {
		playing = false;
	}
	
	public byte[] getRawData() {
		return data;
	}
	
	public byte[] getCurrentData() {
		byte[] modifiedData;
						
		if (startBit == 0 && operations.length() == 0) {
			modifiedData = data;
		} else {	
			byte[] tempData = new byte[data.length];
		
			int byteCount = 0;
			int lastByteWritten = 0;
			Queue<Integer> bitQueue = new ArrayDeque<Integer>();
			
			for(byte b: data) {
				int bits[] = getBitArray(b);
				for(int i = 0; i < bits.length; ++i) {								
					int bitNum = (((byteCount*8)) + i) - startBit;
					if (bitNum >= 0) {											
						int thisBit = bits[i];
						if (operations.length() > 0) {
							char op = getOperation(bitNum);
							switch (op) {
								case 't': break;
								case 's': continue;
								case 'i': thisBit = thisBit == 1 ? 0 : 1;
							}
						}					
						bitQueue.add(thisBit);
					}
				}
				if (bitQueue.size() >= 8) {
					int newByte = 
							bitQueue.poll() << 7 |
							bitQueue.poll() << 6 |
							bitQueue.poll() << 5 |
							bitQueue.poll() << 4 |
							bitQueue.poll() << 3 |
							bitQueue.poll() << 2 |
							bitQueue.poll() << 1 |
							bitQueue.poll() << 0;
					tempData[lastByteWritten++] = (byte)newByte;
				}
				++byteCount;
			}	
			modifiedData = Arrays.copyOf(tempData, lastByteWritten);			
		}
		
		return modifiedData;
	}
}
