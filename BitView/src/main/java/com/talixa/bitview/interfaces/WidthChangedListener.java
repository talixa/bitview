package com.talixa.bitview.interfaces;

public interface WidthChangedListener {

	public void onWidthChanged();
}
