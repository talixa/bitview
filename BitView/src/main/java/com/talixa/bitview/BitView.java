package com.talixa.bitview;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import com.talixa.bitview.analysis.Bias;
import com.talixa.bitview.frames.FrameAbout;
import com.talixa.bitview.frames.FrameImageViewer;
import com.talixa.bitview.frames.FrameOperations;
import com.talixa.bitview.frames.FrameSettings;
import com.talixa.bitview.frames.FrameTextViewer;
import com.talixa.bitview.listeners.BitViewButtonListener;
import com.talixa.bitview.listeners.BitViewKeyListener;
import com.talixa.bitview.listeners.ExitActionListener;
import com.talixa.bitview.shared.BitViewConstants;
import com.talixa.bitview.shared.IconHelper;
import com.talixa.bitview.widgets.BitViewWidget;

public class BitView {

	private static JFrame frame;
	private static BitViewWidget bv;
	
	// use one instance so we can save last folder
	public static JFileChooser fileChooser = new JFileChooser();
	
	// these are declared here so they can be reset
	private static JTextField currentStartText;
	private static JTextField currentOperations;
	
	// label for status info
	private static JLabel statusLabel;
	
	private static void createAndShowGUI() {
		// Create frame
		frame = new JFrame(BitViewConstants.TITLE_MAIN);		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		
		// set icon
		IconHelper.setIcon(frame);		
									
		// create bitview
		bv = new BitViewWidget();
		bv.setDisplayBorder(false);
		bv.setDisplayWidth(BitViewConstants.DEFAULT_WIDTH);	
		bv.setStartBit(BitViewConstants.DEFAULT_START);
		bv.setPreferredSize(new Dimension(5,5));
		
		// Crete menus and toolbar AFTER bv - it is referenced in toolbar
		addMenus();
		addToolbar();
		
		// place bv in scroll pane and add to frame
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setViewportView(bv);
		bv.setAutoscrolls(true);
		frame.add(scrollPane, BorderLayout.CENTER);
		
		// add status label
		statusLabel = new JLabel();
		frame.add(statusLabel, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(BitViewConstants.APP_WIDTH,BitViewConstants.APP_HEIGHT));
		int left = (screenSize.width/2) - (BitViewConstants.APP_WIDTH/2);
		int top  = (screenSize.height/2) - (BitViewConstants.APP_HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);
	}	
	
	private static void addToolbar() {
		JToolBar toolbar = new JToolBar();
		toolbar.setFloatable(false);
		
		// Using images from: http://www.oracle.com/technetwork/java/index-138612.html
		ClassLoader cl = BitView.class.getClassLoader();		
		
		// Width options
		JTextField currentWidthText = new JTextField(String.valueOf(BitViewConstants.DEFAULT_WIDTH));		
		currentWidthText.addKeyListener(new BitViewKeyListener(BitViewKeyListener.SET_WIDTH, bv, currentWidthText));
				
		JButton decreaseWidthButton = new JButton(new ImageIcon(cl.getResource("res/left.gif")));
		decreaseWidthButton.setToolTipText("Decrease Display Width");
		decreaseWidthButton.addActionListener(new BitViewButtonListener(BitViewButtonListener.DECREASE_WIDTH, bv, currentWidthText));				
		
		JButton increaseWidthButton = new JButton(new ImageIcon(cl.getResource("res/right.gif")));
		increaseWidthButton.setToolTipText("Increase Display Width");
		increaseWidthButton.addActionListener(new BitViewButtonListener(BitViewButtonListener.INCREASE_WIDTH, bv, currentWidthText));
				
		JButton playButton = new JButton(new ImageIcon(cl.getResource("res/play.gif")));
		playButton.setToolTipText("Automatically Increment Display Width");
		playButton.addActionListener(new BitViewButtonListener(BitViewButtonListener.PLAY, bv, currentWidthText));
		
		JButton stopButton = new JButton(new ImageIcon(cl.getResource("res/stop.gif")));
		stopButton.setToolTipText("Stop Automatic Increment");
		stopButton.addActionListener(new BitViewButtonListener(BitViewButtonListener.STOP, bv));
		
		// Start options
		currentStartText = new JTextField(String.valueOf(BitViewConstants.DEFAULT_START));		
		currentStartText.addKeyListener(new BitViewKeyListener(BitViewKeyListener.SET_START, bv, currentStartText));
			
		JButton decreaseStartButton = new JButton(new ImageIcon(cl.getResource("res/left.gif")));
		decreaseStartButton.setToolTipText("Left Shift Start Bit");
		decreaseStartButton.addActionListener(new BitViewButtonListener(BitViewButtonListener.DECREMENT_START, bv, currentStartText));
		
		JButton increaseStartButton = new JButton(new ImageIcon(cl.getResource("res/right.gif")));
		increaseStartButton.setToolTipText("Right Shift Start Bit");
		increaseStartButton.addActionListener(new BitViewButtonListener(BitViewButtonListener.INCREMENT_START, bv, currentStartText));
		
		// Size options
		JButton increaseSizeButton = new JButton(new ImageIcon(cl.getResource("res/up.gif")));
		increaseSizeButton.setToolTipText("Increase Bit Display Size");
		increaseSizeButton.addActionListener(new BitViewButtonListener(BitViewButtonListener.INCREMENT_SIZE, bv));
		
		JButton decreaseSizeButton = new JButton(new ImageIcon(cl.getResource("res/down.gif")));
		decreaseSizeButton.setToolTipText("Decrease Bit Display Size");
		decreaseSizeButton.addActionListener(new BitViewButtonListener(BitViewButtonListener.DECREMENT_SIZE, bv));	
		
		JButton textButton = new JButton(new ImageIcon(cl.getResource("res/text.gif")));
		textButton.setToolTipText("Display As Text");
		textButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FrameTextViewer.createAndShowGUI(bv.getCurrentData());
			}
		});
		
		JButton imgButton = new JButton(new ImageIcon(cl.getResource("res/img.gif")));
		imgButton.setToolTipText("Display As Image");
		imgButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FrameImageViewer.createAndShowGUI(bv.getCurrentData());				
			}
		});
		
		// Operations
		currentOperations = new JTextField();
		JButton applyOperations = new JButton(new ImageIcon(cl.getResource("res/play.gif")));
		applyOperations.setToolTipText("Apply Operations");
		applyOperations.addActionListener(new BitViewButtonListener(BitViewButtonListener.APPLY_OPERATIONS, bv, currentOperations));
				
		toolbar.add(new JLabel("Width: "));
		toolbar.add(decreaseWidthButton);
		toolbar.add(currentWidthText);
		toolbar.add(increaseWidthButton);
		toolbar.add(playButton);
		toolbar.add(stopButton);
		toolbar.addSeparator();
		toolbar.add(new JLabel("Start: "));
		toolbar.add(decreaseStartButton);
		toolbar.add(currentStartText);
		toolbar.add(increaseStartButton);
		toolbar.addSeparator();
		toolbar.add(new JLabel("Size: "));
		toolbar.add(decreaseSizeButton);
		toolbar.add(increaseSizeButton);					
		toolbar.addSeparator();
		toolbar.add(textButton);
		toolbar.add(imgButton);
		toolbar.addSeparator();
		toolbar.add(new JLabel("Ops: "));	
		toolbar.add(currentOperations);
		toolbar.add(applyOperations);
		
		frame.add(toolbar, BorderLayout.NORTH);
	}
	
	private static void addMenus() {
		// Setup file menu
		JMenu fileMenu = new JMenu(BitViewConstants.MENU_FILE);
		fileMenu.setMnemonic(KeyEvent.VK_F);
		JMenuItem openMenuItem = new JMenuItem(BitViewConstants.MENU_OPEN);
		openMenuItem.setMnemonic(KeyEvent.VK_O);
		openMenuItem.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				int result = fileChooser.showOpenDialog(frame);
				if (result == JFileChooser.APPROVE_OPTION) {
					try {
						frame.setTitle(BitViewConstants.TITLE_MAIN + " - " + fileChooser.getSelectedFile().getAbsolutePath());
						bv.setDataFromFile(fileChooser.getSelectedFile());							
						bv.setOperations("");
						currentOperations.setText("");
						bv.setStartBit(BitViewConstants.DEFAULT_START);
						currentStartText.setText(String.valueOf(BitViewConstants.DEFAULT_START));
		
						statusLabel.setText(Bias.calculate(fileChooser.getSelectedFile().getAbsolutePath()).toString());
					} catch (IOException exception) {
						JOptionPane.showMessageDialog(frame, "Error opening file");
					}
				}				
			}
		});
		fileMenu.add(openMenuItem);
		
		JMenuItem exportMenuItem = new JMenuItem(BitViewConstants.MENU_EXPORT);
		exportMenuItem.setMnemonic(KeyEvent.VK_E);
		exportMenuItem.addActionListener(new BitViewButtonListener(BitViewButtonListener.EXPORT, bv));
		fileMenu.add(exportMenuItem);		
		fileMenu.addSeparator();
		
		JMenuItem settingsMenuItem = new JMenuItem(BitViewConstants.MENU_SETTINGS);
		settingsMenuItem.setMnemonic(KeyEvent.VK_S);
		settingsMenuItem.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				FrameSettings.createAndShowGUI(bv);
			}
		});
		fileMenu.add(settingsMenuItem);
		fileMenu.addSeparator();
		
		JMenuItem exitMenuItem = new JMenuItem(BitViewConstants.MENU_EXIT);
		exitMenuItem.setMnemonic(KeyEvent.VK_X);
		exitMenuItem.addActionListener(new ExitActionListener(frame));
		fileMenu.add(exitMenuItem);	
		
		// Setup help menu
		JMenu helpMenu = new JMenu(BitViewConstants.MENU_HELP);
		helpMenu.setMnemonic(KeyEvent.VK_H);

		JMenuItem operationsMenuItem = new JMenuItem(BitViewConstants.MENU_OPERATIONS);
		operationsMenuItem.setMnemonic(KeyEvent.VK_O);
		operationsMenuItem.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				FrameOperations.createAndShowGUI();
			}
		});
		helpMenu.add(operationsMenuItem);
		helpMenu.addSeparator();
		
		JMenuItem aboutMenuItem = new JMenuItem(BitViewConstants.MENU_ABOUT);
		aboutMenuItem.setMnemonic(KeyEvent.VK_A);
		aboutMenuItem.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				FrameAbout.createAndShowGUI(BitView.frame);
			}
		});
		helpMenu.add(aboutMenuItem);
		
		// Add menus to menubar
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);
		menuBar.add(helpMenu);
		frame.setJMenuBar(menuBar);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
